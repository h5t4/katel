#!/usr/bin/python
import os
import time
import math
import sys
import signal
from stepper_0_to_100_prc import *
from thermocouple_0_to_500_deg import *

#import GPIO
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#set 220V relay pin as output
GPIO.setup(17,GPIO.OUT)

#create and start temperature probe ADC instance thread
t = temperature()
t.start()
t.daemon = True

# Create a servo instance thread
s = stepper()
s.start()

#write output into this dir, requires existing out dir
f_name = None
f_lock = False
dir = os.path.join(os.path.dirname(__file__),"out")

#read PID controller tuning values
dirPID = os.path.join(os.path.dirname(__file__),"PID")
fhKp = open(os.path.join(dirPID,"Kp"),"r")
fhKi = open(os.path.join(dirPID,"Ki"),"r")
fhKd = open(os.path.join(dirPID,"Kd"),"r")
fhSP = open(os.path.join(dirPID,"SP"),"r")

def signal_handler(signal, frame):
        t.stop()
        s.stop()
        #s.set_pos(s.read_pos())
        fhKp.close()
        fhKi.close()
        fhKd.close()
        fhSP.close()
        time.sleep(1)
        sys.exit(0)


i = 0.0
fan_now = 0
ts_prev = None
P,I,D = 100,0,0
PID = None
pause = 1
door_min = 0
door_max = 100
boost = 60.0
boost_enable = True

while True:
    signal.signal(signal.SIGINT, signal_handler)  #kill thread, before main
    time.sleep(pause)                             #moving stepper one deg requires aprox. one second. 
    ts_now = int(time.time())
    Kp = float(fhKp.read(10)); fhKp.seek(0)  #1.8 makes door open max 86% min 14% (def: 5.0)
    Ki = float(fhKi.read(10)); fhKi.seek(0)  #0.05 1 degrees per second if error is 20 deg (def: 0.03)
    Kd = float(fhKd.read(10)); fhKd.seek(0)  #5 changes 0.5 deg door pos when ROC is 0.1 deg per 10 ceconds (def:55.0)
    SP = float(fhSP.read(10)); fhSP.seek(0)  #temperature set point (def:180.0)

    temp = t.temp                            #update temp on every iteration, ADC conversion takes aprox one second
    Ic = I

 
    if (temp >= SP * 0.5 ):                      #normal burning process  +/- 10%
       if PID == None:                           #initialize PID and file handler ,program may be started when burning already happening
          PID = int(P + I - D)
          if f_lock != True:
             f_name = str(ts_now)
             fh = open(os.path.join(dir,f_name),"a")
             fh.write("timestamp temperature fan door\n")
             f_lock = True
             fan_now = 1
             GPIO.output(17,GPIO.HIGH)
             boost_enable = False
       
       if (ts_now - int(f_name) <= 3600) and ( boost_enable == True ) :
          SP = SP +  boost - (ts_now - int(f_name)) * boost/3600  #burn higher temperatures within first hour

       if (temp < SP * 0.7) and (ts_now - int(f_name) <= 3600) and ( boost_enable == True ) :  #fan on first hour on low tempertures
          fan_now = 1
          GPIO.output(17,GPIO.HIGH)
       elif(temp >= SP * 0.8) or (ts_now - int(f_name) >= 3600): #allow fan on first hour hours
          fan_now = 0
          GPIO.output(17,GPIO.LOW)
       
       diff = SP -temp
       P  = Kp * diff  + 50                      #P, regulate around 180 deg, level shift door pos by 50%
       #if diff <= 0:
       if diff <= 0:
          if PID <= 12:
             I += Ki/8 * diff - t.get_roc30()/5     #close door gently, too fast - no flame, higher temp ok
          else:
             I += Ki/4 * diff - t.get_roc30()/5     #close door very gently, do not waste energy via chemical loss 
       else:
          I += Ki * diff - t.get_roc30()/5       #open door with normal rate
       D  = Kd * t.get_roc30()                   #D, increase and decrease flow when rate of change is big

       if PID == None:                           #initialize PID and file handler ,program may be started when burning already happening
          PID = int(P + I - D)                   
          if f_lock != True:
             f_name = str(ts_now)
             fh = open(os.path.join(dir,f_name),"a")
             fh.write("timestamp temperature fan door\n")
             f_lock = True

       if int(P + I - D) > PID + 1:              #moving stepper one deg requires aprox. one second.
          PID +=1                                
       elif int(P + I - D) < PID - 1:
          PID -=1
       else:
          PID = int(P + I - D)

       if PID <= door_min:                       #due to leckage burning can actually happen with (almost)closed door 
          PID = door_min
          I = Ic                                 #keep old value, not possable to close door even more
       if PID >= door_max:
          PID = door_max
          I = Ic                                 #keep old value, not possable to open door even more

       print "PID:", P,"+",I,"-",D,"=",PID 
       s.set_pos(PID)                             #use only door to keep temperature around SP, save 200W 
    elif (temp < SP * 0.9 and temp >= 110): 	  #normal burning process - end of burning 
       if f_name != None:
          if (ts_now - int(f_name) <= 3600):     #max airflow within 0..1 hours
             fan_now = 1
             GPIO.output(17,GPIO.HIGH)
       I = 45                                     #ensure open door when starting from low temperature
       s.set_pos(100)  
    elif (temp < 110 and temp >= 90 and t.get_roc300_cum() <  -2.5):     #almost out of fuel- stop fan 
         if f_name != None:
            if (ts_now - int(f_name) >= 3600):                          #assume that burning process takes at least one hour
              fan_now = 0
              GPIO.output(17,GPIO.LOW)                                   #stop fan
              s.set_pos(60)                                              #partially close door
    elif (temp < 90 and temp >= 80 and t.get_roc300_cum() <  -1.6 ):     #out of fuel
         if f_name != None:
            if (ts_now - int(f_name) >=3600):                          #assume that burning process takes at least one hour
               fan_now = 0
               GPIO.output(17,GPIO.LOW)                                  #make sure that fan is stopped
               s.set_pos(20)                                             #close door - almost
    elif temp < 75:                                                     
       if  t. get_roc30()  >  0.052:                                     #before starting burning - detect sudden temp rise
           if f_lock != True:                                            #generate filename if not already done
              f_name = str(ts_now)
              fh = open(os.path.join(dir,f_name),"a")
              fh.write("timestamp temperature fan door\n")
              f_lock = True
              

           fan_now = 1
           GPIO.output(17,GPIO.HIGH)                                    #start fan
           s.set_pos(100)                                               #open door
       else:
           if f_name != None:
              if ts_now - int(f_name) >= 1800:                           #let fan work at least 30 minutes when starting
                 fan_now = 0
                 stepper_now = 0
                 GPIO.output(17,GPIO.LOW)                               #stop fan
                 s.set_pos(0)
                 fh.write("%d %0.2f %d %d\n" % (ts_now,math.ceil(temp*100)/100,fan_now,stepper_now) )
                 fh.flush()
                 fh.close()                                             #close log file  
                 f_name = None           
                 f_lock = False
                 PID = None
           else:                                                        #no burning ever starterd, at least for last 15 minutes
              GPIO.output(17,GPIO.LOW)                                  #stop fan
              s.set_pos(0)

    
    #write to log file 
    if f_name != None:
       fh.write("%d %0.2f %d %d\n" % (ts_now,math.ceil(temp*100)/100,fan_now,s.get_pos()) )
       fh.flush()
    #write some debug info to std.out
    print "Kp,Ki,Kd,SP:", Kp, Ki, Kd, SP
    print "STR:", ts_now, math.ceil(temp*100)/100, fan_now, s.get_pos()     
    print "ROC:", t.get_roc1(), t.get_roc10(), t.get_roc30(), t.get_roc60(), t.get_roc60_cum(), t.get_roc300_cum()
    sys.stdout.flush()                                                   
