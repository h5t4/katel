#!/usr/bin/python
import os
import sys
import time
import RPi.GPIO as GPIO
from threading import Thread


#this class moves door between closed position 0% and open position 100% 
class stepper:
   def __init__(self):
       self.wait_time = 1/float(1000)   #Define wait time
       self.max_step = 50000            #Define stepper range, one turn (360deg) is approx. 4000 steps, 80000 is max with longer thread
       self.pos_wnt = None              #set wanted position whatever it already is
       self.stopped = False

       GPIO.setwarnings(False) #turn off warnings
       GPIO.setmode(GPIO.BCM)  #Use BCM GPIO references instead of physical pin numbers
       self.StepPins = [27,22,23,24] #Physical pins 11,15,16,18 eg. GPIO17,GPIO22,GPIO23,GPIO24

       #Set all pins as output
       for pin in self.StepPins:
         GPIO.setup(pin,GPIO.OUT)
         GPIO.output(pin, False)

   
       # Define sequence
       self.seq1 = [[1,0,0,1],
                    [1,0,0,0],
                    [1,1,0,0],
                    [0,1,0,0],
                    [0,1,1,0],
                    [0,0,1,0],
                    [0,0,1,1],
                    [0,0,0,1]]
   
       self.seq0 = [[0,0,0,0],[0,0,0,0]]
       self.seq = self.seq0  #by default stepper off


       dir = os.path.dirname(__file__)
       stepper_pos = "stepper_pos"
       self.fp = os.path.join(dir,stepper_pos)
       self.f_lock = False

   # Read servo current position	
   def get_pos(self):
       while self.f_lock:               #wait while file write is done, then read
          time.sleep(self.wait_time)
       fh = open(self.fp,"r")
       self.pos_cur=int(fh.read(10))
       fh.close()
       return self.pos_cur

   def set_pos(self,pos_wnt):
       self.pos_wnt = pos_wnt


   def turn(self,step,step_dir):
       #one turn is around 4000 steps
       StepCounter = 0
       for x in range(0, step):
         for pin in range(0, 4):
           xpin = self.StepPins[pin]
           if self.seq[StepCounter][pin]!=0:
             GPIO.output(xpin, True)
           else:
             GPIO.output(xpin, False)
         StepCounter += step_dir
         if (StepCounter>=len(self.seq)):
           StepCounter = 0
         if (StepCounter<0):
           StepCounter = len(self.seq)+step_dir
         time.sleep(self.wait_time)

   def start(self):
       Thread(target=self.update).start()

   def update(self): #Set wanted position 0(closed) 100(fully open)
       if self.pos_wnt == None:
          self.pos_wnt = self.get_pos()
       while True:
          pos_wnt = self.pos_wnt   #use local variable to hold wanted position entire cycle
          if pos_wnt > 100:
             pos_wnt = 100
          if pos_wnt < 0:
             pos_wnt = 0

          fh = open(self.fp,"r")
          pos_cur = int(fh.read(10))
          fh.close()

          pos_err = pos_wnt - pos_cur
          if pos_err != 0:
             if (pos_err > 0 and pos_err <= 100): #cw
                step = (pos_err * self.max_step)/100
                self.seq = self.seq1  #turn stepper on
                step_dir = 1
             elif (pos_err < 0 and pos_err >= -100): #ccw
                step = abs((pos_err * self.max_step)/100)
                self.seq = self.seq1  #turn stepper on
                step_dir = -1
             else:
                step = 0
                self.seq = self.seq0 #turn stepper off
                step_dir = 1

             self.turn(step,step_dir)
             self.f_lock = True
             fh = open(self.fp,"w")
             fh.write(str(pos_wnt))
             fh.close()
             self.f_lock = False
             
           
          time.sleep(self.wait_time)
          if self.stopped:
             return pos_wnt

   def stop(self):
       self.stopped = True

