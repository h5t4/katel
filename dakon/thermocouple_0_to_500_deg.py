from threading import Thread
import os
import time
import math
import sys
import collections
import Adafruit_ADS1x15

#this class measures current thermocouple temperature and calculates temperature rate of change ROC
class temperature:
   def __init__(self):
      self.adc  = Adafruit_ADS1x15.ADS1115()      #Create an ADS1115 ADC (16-bit) instance.
      self.d10  = collections.deque(maxlen=10)    #Hold last maxlen samples, around 10 seconds
      self.d30  = collections.deque(maxlen=30)    #Hold last maxlen samples, around 30 seconds 
      self.d60  = collections.deque(maxlen=60)    #Hold last maxlen samples, around 60 seconds
      self.d300 = collections.deque(maxlen=300)   #Hold last maxlen samples, around 5 minutes
      self.stopped = False                       #stop this thread if True 
      self.GAIN = 1                              #Set ADC gain to +/-4.096V
      self.temp = 0                              #Initial temperature before ADC is started
      self.roc = 0                               #Rate Of Change degrees per second

   def start(self):
      Thread(target=self.update).start()
      time.sleep(5)

   def stop(self):
      self.stopped = True

   def update(self):
      cnt = 88                       #Average ovar this number of samples. It takes around 1s to take 88 samplest. 
      temp = None
      while True:
         if self.stopped:
            return
         dt = time.time()
         value = 0
         for x in range(0, cnt):
              tmp = self.adc.read_adc_difference(0, gain=self.GAIN)
              value += tmp
         voltage = ((float(value)) * 4.096 / 32768)/cnt  #16bit analog digital converter ADS1115
         self.temp = (voltage - 1.25) / 0.005            #K-type thermocouple amplifier AD8495 

         #caclulate Rate of change
         if temp != None:                                   #skip first because of missing previous
            self.roc = (self.temp-temp) / (time.time() - dt)
            self.d10.append(self.roc)
            self.d30.append(self.roc)
            self.d60.append(self.roc)
            self.d300.append(self.roc)
         temp = self.temp

   #convience functions to get ROC over different time intervals  
   def get_roc1(self):
       return self.roc

   def get_roc10(self):
       return sum(self.d10)/len(self.d10)

   def get_roc30(self):
       return sum(self.d30)/len(self.d30)

   def get_roc60(self):
       return sum(self.d60)/len(self.d60)
   
   def get_roc60_cum(self):
       return sum(self.d60)

   def get_roc300_cum(self):
       return sum(self.d300)
